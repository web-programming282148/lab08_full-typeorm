import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productsRepository.save(createProductDto);
  }

  findAll() {
    return this.productsRepository.find({ relations: { type: true } });
  }

  async findOne(id: number) {
    await this.productsRepository.findOneByOrFail({ id });

    return this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    let product = await this.productsRepository.findOneByOrFail({ id });
    // await this.productsRepository.update(id, updateProductDto);
    await this.productsRepository.update(id, {
      ...product,
      ...updateProductDto,
    });
    product = await this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return product;
  }

  async remove(id: number) {
    const removeProduct = await this.productsRepository.findOneByOrFail({ id });
    await this.productsRepository.remove(removeProduct);
    return removeProduct;
  }
}

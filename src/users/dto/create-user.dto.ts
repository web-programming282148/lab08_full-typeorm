// import { IsNotEmpty } from 'class-validator';

import { IsNotEmpty } from 'class-validator';
import { Role } from 'src/roles/entities/role.entity';

// export class CreatePromotionDto {
//   @IsNotEmpty()
//   name: string;

//   @IsNotEmpty()
//   description: string;

//   @IsNotEmpty()
//   start_date: string;

//   @IsNotEmpty()
//   end_date: string;
// }

export class CreateUserDto {
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  gender: string;

  @IsNotEmpty()
  created: Date;

  @IsNotEmpty()
  roles: Role[];
}

// "id": 2,
//         "email": "user",
//         "password": "Pass@1234",
//         "gender": "female",
//         "created": "2024-02-19T15:25:44.000Z",
//         "update": "2024-02-19T15:25:44.000Z",
//         "roles": [
//             {
//                 "id": 2,
//                 "name": "user",
//                 "created": "2024-02-19T15:25:41.000Z",
//                 "update": "2024-02-19T15:25:41.000Z"
//             }
//         ]
//     }

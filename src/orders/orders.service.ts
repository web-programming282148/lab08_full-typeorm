import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
import { UpdateOrderDto } from './dto/update-order.dto';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createOrderDto: CreateOrderDto) {
    // const order = new Order();
    // const user = this.usersRepository.findOneBy({ id: createOrderDto.user.id });
    // order.total = 0;
    // order.qty = 0;
    // order.orderItems = [];

    // //put values in to Order
    // for (const io of createOrderDto.orderItems) {
    //   const orderItems = new OrderItem();
    //   orderItems.product
    // }

    return this.ordersRepository.save(createOrderDto);
  }

  async findAll() {
    return await this.ordersRepository.find({
      relations: { orderItems: true, user: true },
    });
  }

  async findOne(id: number) {
    return await this.ordersRepository.findOne({
      where: { id },
      relations: { orderItems: true, user: true },
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    this.ordersRepository.findOneByOrFail({ id });
    await this.ordersRepository.update(id, updateOrderDto);
    const updateOrder = await this.ordersRepository.findOneBy({ id });
    return updateOrder;
  }

  async remove(id: number) {
    const delOrder = await this.ordersRepository.findOneByOrFail({ id });
    await this.ordersRepository.remove(delOrder);

    return delOrder;
  }
}

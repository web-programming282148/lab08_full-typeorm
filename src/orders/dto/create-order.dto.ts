import { User } from 'src/users/entities/user.entity';
import { OrderItem } from '../entities/orderItem.entity';
import { IsNotEmpty } from 'class-validator';

export class CreateOrderDto {
  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  qty: number;

  @IsNotEmpty()
  created: Date;

  @IsNotEmpty()
  orderItems: OrderItem[];

  @IsNotEmpty()
  user: User;
}
